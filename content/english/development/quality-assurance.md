---
title: "Quality Assurance"
draft: false
# page title background image
bg_image: "images/backgrounds/page-title.jpg"
# meta description
description : ""
# development download link
download_link : "contact"
# type
type: "development"
image: "https://www.path-tec.com/wp-content/uploads/2018/08/Quality-57624940_xl-1024x683.jpg"
---

#### Skilled team of professionals
at an early stage by testing, retesting and regression testing on different browsers, multiple operating systems and various devices.

![link](https://www.path-tec.com/wp-content/uploads/2018/08/Quality-57624940_xl-1024x683.jpg)

**We consider our work as our craft. So, like any craftsman we keep on improving the quality of our work to meet the client and user satisfaction.**

**We're a remote first company with a cohesive team, working asynchronously to help people across the globe to build and scale their dream projects.**

**We prefer documentation over talks and over communicating things to make them clear to everyone. Two44software believes in small teams as they are fast and nimble, which means less management and getting more things done.**

**We follow agile methodology which involves sprint planning, daily updates, sprint retrospective, client demo, UAT apart from requirement gathering, design work, feature implementation,testing, deployment and maintenance.**

