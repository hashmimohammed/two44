---
title: "Cordova App Development"
draft: false
# page title background image
bg_image: "images/backgrounds/page-title.jpg"
# meta description
description : "A product and service based company with an energetic team of passionate, self-driven individuals with working experience with many project."
# type
type: "cordava_app_development"
---

### Cordova

Two44 software solution use to work with Cordova, which is known as PhoneGap is an open- source framework designed to let developers create mobile apps using standards-based web
technologies.

We can build an application with Cordova, the UI is designed in HTML5, style is added with
CSS3, and the application logic is written in javaScript.